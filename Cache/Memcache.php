<?php
/**
 * File:       Memcache.php
 *
 * Memcache class
 *
 * @package    IATSTUTI
 * @subpackage Cache
 * @copyright  2012 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
namespace IATSTUTI\Common\Cache;

use IATSTUTI\Common\Cache\Exception\MemcacheException;

class Memcache
{
    /**#@+
     * Private class variable
     *
     * @access private
     */
    /** Store memcache instance */
    private $memcache;
    /**#@-*/


    /**
     * Class constructor
     *
     * @throws MemcacheException If unable to connect to memcache host
     * @param  string $host Host to connect to
     * @param  string $port Port to connect to
     * @return bool
     */
    public function __construct($host, $port)
    {
        $this->memcache = memcache_connect($host, $port);

        if ( $this->memcache === false ) {
            throw new MemcacheException('Unable to connect to memcache host');
        }

        return true;
    }


    /**
     * Add an item to the server
     *
     * @param  string $key   The key that will be associated with the item
     * @param  mixed  $value The variable to store
     * @param  int    $ttl   Expiration time of the item
     * @return bool
     */
    public function add($key, $value, $ttl)
    {
        if ( intval($ttl) !== $ttl ) {
            return $this->memcache->add($key, $value);
        }

        return $this->memcache->add($key, $value, null, $ttl);
    }


    /**
     * Get an item from the server
     *
     * @param  string $key The key or array of keys to fetch
     * @return string|bool Returns string associated with key if found. False
     *                     on failure, key not found or key is an empty array.
     */
    public function get($key)
    {
        return $this->memcache->get($key);
    }


    /**
     * Delete an item from the server
     *
     * @param  string $key They key associated with the item to delete
     * @return bool
     */
    public function del($key)
    {
        return $this->memcache->delete($key);
    }
}
