<?php
/**
 * File:       Registry.php
 *
 * Common registry
 *
 * @package    IATSTUTI
 * @subpackage Common
 * @copyright  2012 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
namespace IATSTUTI\Common;

class Registry
{
    /**#@+
     * Private class property
     *
     * @access private
     */
    /**
     * Store registry properties
     *
     * @var array
     */
    private $data = array();


    /**
     * Overload set method
     *
     * @param string $key   Property key
     * @param mixed  $value Property value
     */
    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }


    /**
     * Overload get method
     * @param  string $key Property key to retireve
     * @return mixed
     */
    public function __get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }


    /**
     * Check if a key exists
     *
     * @param  string  $key Property key to check
     * @return boolean
     */
    public function has($key)
    {
        return isset($this->data[$key]);
    }
}
