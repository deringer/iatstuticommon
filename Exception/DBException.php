<?php
/**
 * File:       DB.php
 *
 * DB Exception class
 *
 * @package    IATSTUTI
 * @subpackage Exception
 * @copyright  2012 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
namespace IATSTUTI\Common\Exception;

class DBException extends \Exception
{
    /**
     * Class constructor. Invokes parent only.
     *
     * @param string  $message  Message to be thrown
     * @param integer $code     Error code to be thrown
     * @param object  $previous Previous exception (for stacktracing)
     */
    public function __construct(
        $message = null,
        $code = 0,
        Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
