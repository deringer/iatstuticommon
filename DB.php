<?php
/**
 * File:       DB.php
 *
 * Database class
 *
 * @package    IATSTUTI
 * @subpackage Common
 * @copyright  2012 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
namespace IATSTUTI\Common;

use IATSTUTI\Common\Exception\DBException;

class DB
{
    /**#@+
     * Private class property
     *
     * @access private
     */
    /** Database driver instance */
    private $driver;
    /**#@-*/


    /**
     * Class constructor
     *
     * @param string $driver   Database driver to use
     * @param string $hostname Hostname to connect to
     * @param string $username Username to authenticate with
     * @param string $password Password to authenticate with
     * @param string $database Database name to connect to
     */
    public function __construct(
        $driver,
        $hostname,
        $username,
        $password,
        $database
    ) {
        switch ($driver) {
            case DB_DRIVER_PDO:
                $this->driver = new DB\PDO(
                    DB_HOST,
                    DB_USER,
                    DB_PASS,
                    DB_NAME
                );
                break;
            default:
                throw new \Exception(sprintf('Unknown driver %s', $driver));
                break;
        }
    }


    /**
     * Execute a raw SQL query
     *
     * @param  string $query SQL query to be executed
     * @return object
     */
    public function query($query)
    {
        return $this->driver->query($query);
    }


    /**
     * Perform a select query
     *
     * @throws DBException      On database error
     * @param  string $table    Table to select from
     * @param  array  $fields   Fields to select
     * @param  array  $keys     Constraints to limit by
     * @param  string $order_by Key to order by
     * @param  string $sort     Direction to order by
     * @return array  Results from query
     */
    public function select(
        $table,
        $fields,
        $keys,
        $order_by = null,
        $sort = null
    ) {
        return $this->driver->select($table, $fields, $keys, $order_by, $sort);
    }

    /**
     * Perform an insert query
     *
     * @param  string $table  Table to insert into
     * @param  array  $values Key/value pairs of values to insert
     * @return int    Last insert ID
     */
    public function insert($table, $values)
    {
        return $this->driver->insert($table, $values);
    }


    /**
     * Escape a value to be used in an SQL query
     *
     * @param  string $value String to be escaped
     * @return string
     */
    public function escape($value)
    {
        return $this->driver->escape($value);
    }


    /**
     * Get the number of returned rows
     *
     * @return int
     */
    public function getNumRows()
    {
        return $this->driver->getNumRows();
    }


    /**
     * Return a count of the affected rows (UPDATE, DELETE)
     *
     * @return int
     */
    public function countAffected()
    {
        return $this->driver->countAffected();
    }


    /**
     * Retrieve the last inserted primary key identifier
     *
     * @return int
     */
    public function getLastId()
    {
        return $this->driver->getLastId();
    }
}
