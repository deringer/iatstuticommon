<?php
/**
 * File:       PDO.php
 *
 * PDO DB implementation
 *
 * @package    IATSTUTI
 * @subpackage DB
 * @copyright  2012 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
namespace IATSTUTI\Common\DB;

use IATSTUTI\Common\Exception\DBException;

class PDO
{
    /**#@+
     * Private class variable
     *
     * @access private
     */
    /** PDO instance */
    private $pdo;

    /** Number of affected rows */
    private $affected = 0;

    /** Number of rows returned */
    private $num_rows = 0;
    /**#@-*/


    /**
     * Class constructor
     *
     * @throws DBException on database error
     * @param  string $hostname Hostname to connect to
     * @param  string $username Username to authenticate with
     * @param  string $password Password to authenticate with
     * @param  string $database Database name to connect to
     */
    public function __construct($hostname, $username, $password, $database)
    {
        $dsn = sprintf('mysql:host=%s;dbname=%s', $hostname, $database);

        try {
            $this->pdo = new \PDO($dsn, $username, $password);
            $this->pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
            $this->pdo->setAttribute(
                \PDO::ATTR_ERRMODE,
                \PDO::ERRMODE_EXCEPTION
            );
        } catch ( PDOException $e ) {
            throw new DBException($e->getMessage());
        }
    }


    /**
     * Execute a raw SQL query
     *
     * @throws DBException On database error
     * @param  string $sql The SQL query to be executed
     * @return object
     */
    public function query($sql)
    {
        $sql = trim($sql);

        try {
            $sth = $this->pdo->prepare($sql);
            $sth->execute();

            $this->affected = $sth->rowCount();

            // If this is not an update or delete query
            if (
                stripos($sql, 'update') !== 0 && stripos($sql, 'delete') !== 0
            ) {
                $result = $sth->fetchAll(\PDO::FETCH_ASSOC);

                $counter = 0;
                $data    = array();

                foreach ($result as $r) {
                    $data[$counter] = $r;

                    $counter++;
                }

                $result         = new \stdClass();
                $result->rows   = $data;
                $this->num_rows = $counter;

                return $result;
            } else {
                return $this->affected;
            }
        } catch ( Exception $e ) {
            throw new DBException($e->getMessage());
        }
    }


    /**
     * Perform a select query
     *
     * @throws DBException      On database error
     * @param  string $table    Table to select from
     * @param  array  $fields   Fields to select
     * @param  array  $keys     Constraints to limit by
     * @param  string $order_by Key to order by
     * @param  string $sort     Direction to order by
     * @return array  Results from query
     */
    public function select(
        $table,
        $fields,
        $keys,
        $order_by = null,
        $sort = null
    ) {
        if ( ! is_array($fields) || count($fields) == 0 ) {
            throw new DBException('No fields to select');
        }

        try {
            $query = sprintf(
                'SELECT `%s` FROM `%s`',
                join('`, `', $fields),
                $table
            );

            if ( is_array($keys) && count($keys) > 0 ) {
                $limiters = array();

                foreach ($keys as $key => $value) {
                    $limiters[] = sprintf(
                        '`%s` = %s',
                        $key,
                        $this->pdo->quote($value)
                    );
                }

                $query .= sprintf(' WHERE %s', join(' AND ', $limiters));
            }

            if ( ! is_null($order_by) && ! is_null($sort) ) {
                $query .= sprintf(' ORDER BY %s %s', $order_by, $sort);
            }

            $result = $this->query($query);

            return $result->rows;
        } catch ( PDOException $e ) {
            throw new DBException($e->getMessage());
        }
    }


    /**
     * Perform an insert query
     *
     * @param  string $table  Table to insert into
     * @param  array  $values Key/value pairs of values to insert
     * @return int Last insert ID
     */
    public function insert($table, $query_values)
    {
        $values = $columns = $bind = array();

        foreach ($query_values as $column => $value) {
            $column = trim($column);
            $quote  = true;

            // Interpret this value literally
            if ( substr($column, 0, 1) == '!' ) {
                $column = substr($column, 1);
                $quote  = false;
            }

            $columns[] = sprintf('`%s`', $column);

            if ( is_null($value) ) {
                $bind[] = 'NULL';
            } elseif ( $quote ) {
                $bind[]   = '?';
                $values[] = $value;
            } else {
                $bind[] = $value;
            }
        }

        try {
            $query = sprintf(
                'INSERT INTO `%s` ( %s ) VALUES ( %s )',
                $table,
                join(', ', $columns),
                join(', ', $bind)
            );

            $sth = $this->pdo->prepare($query);
            $sth->execute($values);

            return $this->getLastId();
        } catch ( Exception $e ) {
            throw new DBException($e->getMessage());
        }
    }


    /**
     * Escape a value to be used in an SQL query
     *
     * @param  string $value String to be escaped
     * @return string
     */
    public function escape($value)
    {
        return $this->pdo->quote($value);
    }


    /**
     * Get the number of returned rows
     *
     * @return int
     */
    public function getNumRows()
    {
        return $this->num_rows;
    }


    /**
     * Return a count of the affected rows (UPDATE, DELETE)
     *
     * @return int
     */
    public function countAffected()
    {
        return $this->affected;
    }


    /**
     * Retrieve the last inserted primary key identifier
     *
     * @return int
     */
    public function getLastId()
    {
        return $this->pdo->lastInsertId();
    }


    /**
     * Class destructor
     */
    public function __destruct()
    {
        $this->affected = 0;
    }
}
