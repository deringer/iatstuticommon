<?php
/**
 * File:       Cache.php
 *
 * Cache class
 *
 * @package    IATSTUTI
 * @subpackage Cache
 * @copyright  2012 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
namespace IATSTUTI\Common;

use IATSTUTI\Common\Cache\Memcache;
use IATSTUTI\Common\Exception\CacheException;

class Cache
{
    /**#@+
     * Private class variable
     *
     * @access private
     */
    /** Caching engine to use */
    private $engine;
    /**#@-*/


    /**
     * Class constructor
     *
     * @param  string $engine Engine to be used
     * @param  string $host   For engines requiring it, host to connect to
     * @param  string $port   For engines requiring it, port to connect to
     */
    public function __construct($engine, $host = null, $port = null)
    {
        switch ($engine) {
            case CACHE_ENGINE_MEMCACHE:
                if ( is_null($host) ) {
                    throw new CacheException('host is required for memcache');
                }

                if ( is_null($port) ) {
                    throw new CacheException('port is required for memcache');
                }

                try {
                    $this->engine = new Memcache($host, $port);
                } catch ( MemcacheException $e ) {
                    throw new CacheException($e->getMessage());
                }
                break;
            default:
                throw new CacheException('Unknown engine' . $engine);
                break;
        }
    }


    /**
     * Add an item to the caching engine
     *
     * @param  string $key   The key that will be associated with the item
     * @param  mixed  $value The variable to store
     * @param  int    $ttl   Expiration time of the item
     * @return bool
     */
    public function add($key, $value, $ttl)
    {
        return $this->engine->add($key, $value, $ttl);
    }


    /**
     * Get an item from the server
     *
     * @param  string $key The key or array of keys to fetch
     * @return string|bool Returns string associated with key if found. False
     *                     on failure, key not found or key is an empty array.
     */
    public function get($key)
    {
        return $this->engine->get($key);
    }


    /**
     * Delete an item from the server
     *
     * @param  string $key They key associated with the item to delete
     * @return bool
     */
    public function del($key)
    {
        return $this->engine->del($key);
    }
}
